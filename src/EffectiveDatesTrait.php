<?php

namespace Drupal\effective_dates;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

trait EffectiveDatesTrait {

  public static function effectiveDatesBaseFieldDefinitions(EntityTypeInterface $entity_type) {
    return [
      'effective_dates' => BaseFieldDefinition::create('daterange')
        ->setLabel(t('Effective Dates'))
        ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE)
        ->setDefaultValue('')
        ->setDisplayOptions('view', [
          'type' => 'daterange_default',
          'weight' => 10,
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayOptions('form', [
          'type' => 'daterange_default',
          'weight' => 10,
        ])
        ->setDisplayConfigurable('form', TRUE),
    ];
  }

  public function getIsEffective() {
    $today = new DrupalDateTime('now', DATETIME_STORAGE_TIMEZONE);
    $today = $today->format(DATETIME_DATE_STORAGE_FORMAT);
    if ($value = $this->effective_dates->value) {
      if ($value > $today) {
        return FALSE;
      }
    }
    if ($end_value = $this->effective_dates->end_value) {
      if ($end_value < $today) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
