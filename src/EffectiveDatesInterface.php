<?php

namespace Drupal\effective_dates;

interface EffectiveDatesInterface {

  public function getIsEffective();

}
