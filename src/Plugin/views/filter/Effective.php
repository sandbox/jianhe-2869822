<?php

namespace Drupal\effective_dates\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\BooleanOperator;

/**
 * Filter handler for the effective data.
 *
 * @ViewsFilter("effective")
 */
class Effective extends BooleanOperator {

}
